import {makeAutoObservable} from "mobx";

export default class NewsStore {
    constructor() {
        this._categories = []
        this._news = []
        this._comments = []
        this._selectedCategory = {}
        makeAutoObservable(this);
    }

    setSelectedCategory(category) {
        this._selectedCategory = category
    }

    setCategories(categories) {
        this._categories = categories;
    }

    setNews(news) {
        this._news = news;
    }

    removeComment(comment) {
        this._comments.splice(comment, 1)
    }

    setComments(comments) {
        this._comments = comments;
    }

    get comments() {
        return this._comments;
    }

    get categories() {
        return this._categories
    }

    get news() {
        return this._news
    }

    get selectedCategory() {
        return this._selectedCategory
    }
}
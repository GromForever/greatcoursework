import {makeAutoObservable} from "mobx";

export default class UserEntity {
    constructor() {
        this._isAuth = false;
        this._user = {};
        this._loadedUser = {}
        makeAutoObservable(this);
    }

    setIsAuth(bool) {
        this._isAuth = bool;
    }

    setLoadedUser(user) {
        this._loadedUser = user;
    }

    setUser(user) {
        this._user = user;
    }

    get loadedUser() {
        return this._loadedUser
    }


    get isAuth() {
        return this._isAuth
    }

    get user() {
        return this._user;
    }
}
import Admin from "./pages/Admin";
import {ADMIN_ROUTE, LOGIN_ROUTE, MAIN_PAGE_ROUTE, NEWS_ROUTE, PROFILE_ROUTE, REGISTER_ROUTE} from './utils/consts';
import Profile from "./pages/Profile";
import Auth from "./pages/Auth";
import News from "./pages/News";
import Main from "./pages/Main";
import OneNews from "./pages/OneNews";
import NewsCreatePage from "./pages/NewsCreatePage";

export const AuthRoutes = [
    {
        path: ADMIN_ROUTE,
        component: Admin
    },
    {
        path: PROFILE_ROUTE,
        component: Profile
    },
    {
        path: NEWS_ROUTE + '/create',
        component: NewsCreatePage
    }
];
export const PublicRoutes = [
    {
        path: LOGIN_ROUTE,
        component: Auth
    },
    {
        path: REGISTER_ROUTE,
        component: Auth
    },
    {
        path: NEWS_ROUTE + '/:id',
        component: OneNews
    },
    {
        path: NEWS_ROUTE,
        component: News
    },
    {
        path: MAIN_PAGE_ROUTE,
        component: Main
    }
]

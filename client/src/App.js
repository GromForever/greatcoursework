import {BrowserRouter} from "react-router-dom";
import AppRoutes from "./components/AppRoutes";
import NavBar from "./components/Navbar";
import {observer} from "mobx-react-lite";
import {useContext, useEffect, useState} from "react";
import {Context} from "./index";
import {check} from "./http/userAPI";
import {Spinner} from "react-bootstrap";

const App = observer(() => {
    const {user} = useContext(Context)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        if (localStorage.getItem('token')) {
            check().then(data => {
                user.setUser(data)
                console.log(data)
                user.setIsAuth(true)
            }).finally(() => setLoading(false))
        } else {
            setLoading(false)
        }
    }, [user])

    if (loading) {
        return <Spinner animation={"grow"}/>
    }
  return (
    <BrowserRouter>
        <NavBar/>
      <AppRoutes/>
    </BrowserRouter>
  );
})

export default App;

import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import UserEntity from "./storage/UserEntity";
import NewsStore from "./storage/NewsStore";

export const Context = createContext(null);

ReactDOM.render(
    <Context.Provider value={{
        user: new UserEntity(),
        news: new NewsStore()
    }}>
        <App />
    </Context.Provider>,
  document.getElementById('root')
);

import {$authHost, $host} from "./index";

export const fetchNews = async () => {
    const {data} = await $host.get('api/news')
    return data
}

export const fetchOneNews = async(id) => {
    const {data} = await $host.get('api/news/' + id)
    return data
}

export const fetchCommentsToNews = async (id) => {
    const {data} = await $host.get('api/comments/getbynews/' + id)
    return data
}

export const CreateComment = async (newsID, commentText, userID) => {
    const {data} = await $authHost.post('api/comments/create', {commentText: commentText, IDNews: newsID, user: userID})
    return data
}

export const RemoveCommentFromNews = async (ID) => {
    const {data} = await $authHost.post('api/comments/remove', {id: ID})
    return data
}

export const EditCommentFromNews = async (ID, editedComment) => {
    const {data} = await $authHost.put('api/comments/edit/' + ID, {commentText: editedComment})
    return data
}

export const CreateNews = async (News) => {
    const {data} = await $authHost.post('api/news/status/create', News)
    return data
}

export const getCategories = async () => {
    const {data} = await $host.get('api/category');
    return data
}

export const createCategory = async(CategoryName) => {
    const {data} = await $authHost.put('api/category/create', {CategoryName: CategoryName})
    return data;
}

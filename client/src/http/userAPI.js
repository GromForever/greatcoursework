import {$authHost, $host} from "./index";
import jwt_decode from "jwt-decode";

export const registration = async (email, password, login) => {
    const {data} = await $host.post('api/user/registration', {email, password,login})
    localStorage.setItem('token', data.token)
    console.log(data.token)
    return jwt_decode(data.token)
}

export const getUser = async (id) => {
    const {data} = await $host.get('api/user/' + id)
    return data
}

export const login = async (email, password) => {
    const {data} = await $host.post('api/user/login', {email, password})
    console.log(jwt_decode(data.token))
    localStorage.setItem('token', data.token)
    return jwt_decode(data.token)
}

export const check = async () => {
    const {data} = await $authHost.get('api/user/auth' )
    localStorage.setItem('token', data.token)
    return jwt_decode(data.token)
}

export const edit = async (formData) => {
    const {data} = await $authHost.put('api/user/edit', formData)
    return data
}

export const fetchUsers = async () => {
    const {data} = await $authHost.get('api/user/get')
    return data
}
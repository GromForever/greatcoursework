import {$host} from "./index";

export const fetchRoles = async () => {
    const {data} = await $host.get('api/role/get')
    return data
}

export const fetchPermissions = async() => {
    const {data} = await $host.get('api/permissions/get')
    return data
}

export const fetchPermissionsByRole = async(id) => {
    const {data} = await $host.post('api/role/getpermissions', {role: id})
    return data
}

export const addPermissionsToRole = async(id, permission) => {
    const {data} = await $host.post('api/role/add', {role: id, permission: permission})
    return data
}

export const removePermissionsToRole = async(id, permission) => {
    const {data} = await $host.post('api/role/remove', {role: id, permission: permission})
    return data
}

export const removeRoleFromUser = async(user, role) => {
    const {data} = await $host.post('api/role/removerole', {user: user, role: role})
    return data
}

export const addRoleToUser = async(user, role) => {
    const {data} = await $host.post('api/role/addrole', {user: user, role: role})
    return data
}

export const getRoles = async(user) => {
    const {data} = await $host.post('api/role/getroles', {user: user})
    return data
}
import React, {useEffect, useState} from 'react';
import {
    addPermissionsToRole,
    fetchPermissions,
    fetchPermissionsByRole,
    fetchRoles,
    removePermissionsToRole
} from "../http/rolesAPI";
import {Button, Dropdown, Form, Modal, ModalBody, ModalFooter, ModalTitle} from "react-bootstrap";
import ModalHeader from "react-bootstrap/ModalHeader";
import DropdownToggle from "react-bootstrap/DropdownToggle";
import DropdownMenu from "react-bootstrap/DropdownMenu";
import DropdownItem from "react-bootstrap/DropdownItem";

const ModalPermissions = ({handleShow, handleClose, show}) => {
    const [selectedRole, setSelectedRole] = useState({})
    const [permissions, setPermissions] = useState([])
    const [roles, setRoles] = useState([])
    const [permissionsOfRole, setPermissionsOfRole] = useState([])
    useEffect(() => {
        fetchPermissions().then(data => setPermissions(data)).catch(data => alert(data))
        fetchRoles().then(data => setRoles(data)).catch(data => alert(data))
    }, [])

    const getPermissionsByRole = async (id) => {
        const permissions = await fetchPermissionsByRole(id)
        setPermissionsOfRole(permissions)
    }

    const CheckboxCheck = (permission) => {
        for (let i = 0; i < permissionsOfRole.length; i++) {
            let tempResult = permissionsOfRole[i].rolepermissions.permissionIDPermission === permission.IDPermission;
            if (tempResult === true) {
                return tempResult;
            }
        }
        return false;
    }

    const onChange = async (checkboxValue, role, permission) => {
        if (checkboxValue) {
            addPermissionsToRole(role, permission).then(data => {
                getPermissionsByRole(role).then(data1 => {setRoles(data1)}).catch(data => {alert(data)})
            }).catch(data => {alert(data)})
        } else {
            removePermissionsToRole(role, permission).then(data => {
                getPermissionsByRole(role).then(data1 => {setRoles(data1)}).catch(data => {alert(data)})
            }).catch(data => {alert(data)})
        }
    }

    return (
        <div>
            <Modal show={show} onHide={handleClose}>
                <ModalHeader>
                    <ModalTitle>Разрешения роли</ModalTitle>
                </ModalHeader>
                <ModalBody>
                    <div>
                        <Dropdown className="mb-4">
                            <DropdownToggle>
                                {selectedRole.IDRole ? selectedRole.RoleName : 'Выберите роль'}
                            </DropdownToggle>
                            <DropdownMenu>
                                {roles && roles.map(role => {
                                    return <DropdownItem onClick={(e) => {
                                        setSelectedRole(role);
                                        getPermissionsByRole(role.IDRole)}
                                    } key={role.IDRole}>{role.RoleName}</DropdownItem>
                                })}
                            </DropdownMenu>
                        </Dropdown>
                        {permissions.map(permission => {
                            return <Form.Check key={permission.IDPermission} type={"checkbox"} checked={CheckboxCheck(permission)} onChange={(e) => onChange(e.target.checked, selectedRole.IDRole, permission.IDPermission)} label={permission.PermissionName}>

                            </Form.Check>
                        })}
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={handleClose}>Отмена</Button>
                </ModalFooter>
            </Modal>
        </div>
    )
};

export default ModalPermissions;
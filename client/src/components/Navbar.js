import React, {useContext} from 'react';
import {Button, Container, Nav, Navbar} from "react-bootstrap";
import {NavLink, useHistory} from "react-router-dom";
import {LOGIN_ROUTE, MAIN_PAGE_ROUTE} from "../utils/consts";
import '../style/style.css'
import {Context} from "../index";

import {observer} from "mobx-react-lite";

const NavBar = observer(() => {
    const history = useHistory()

    const logOut = () => {
        user.setUser({})
        user.setIsAuth(false)
        localStorage.removeItem('token');
    }
    const {user} = useContext(Context);
    return (
            <Navbar bg="light" variant="light">
                <Container>
                    <NavLink style={{color: 'black'}} className="navbar-brand-link" to={MAIN_PAGE_ROUTE}>GromService</NavLink>
                    <Nav className="ml-auto navbar-links">
                        <Nav.Link onClick={() => history.push('/')}>Главная</Nav.Link>
                        <Nav.Link onClick={() => history.push('/news')}>Новости</Nav.Link>
                        {user.isAuth ?
                            <div className="auth-block d-flex">
                                <Button variant={"outline-dark"} onClick={() => history.push('/profile')} className="mr-2">Профиль</Button>
                                <Button variant={"outline-dark"} onClick={logOut}>Выйти</Button>
                            </div>
                            :
                            <Button variant={"outline-dark"} onClick={() => history.push(LOGIN_ROUTE)}>Авторизация</Button>
                        }
                    </Nav>
                </Container>
            </Navbar>
    );
});

export default NavBar;
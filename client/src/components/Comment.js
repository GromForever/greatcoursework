import React, {useContext, useEffect, useState} from 'react';
import {getUser} from "../http/userAPI";
import {Context} from "../index";
import {RemoveCommentFromNews} from "../http/newsAPI";
import {observer} from "mobx-react-lite";

const Comment = observer(({comment, commentCount, handleShow, setCurrentComment}) => {
    const [user, setUser] = useState({})
    const authUser = useContext(Context);
    const commentRemove = async () => {
        RemoveCommentFromNews(comment.IDComment)
            .then((data) =>
            {
                alert(data.Result)
                authUser.news.removeComment(commentCount)
                console.log(authUser.news.comments)
            })
            .catch(data => alert(data.message))
    }
    console.log(comment)
    useEffect( () => {
        getUser(comment.userIDUser).then((data) => setUser(data))
    }, [comment.userIDUser])
    return (
        <div className="d-flex flex-row comment-row m-t-0">
            <div className="p-2"><img src={process.env.REACT_APP_API_URL + user.userimg} alt="user" width="50"
                                      className="rounded-circle"/></div>
            <div className="comment-text w-100">
                <h6 className="font-medium">{user.UserName}</h6> <span className="m-b-15 d-block">{comment.CommentText} </span>
                <div className="comment-footer"><span className="text-muted float-right">{comment.CommentDate.replace(/:\d{2}\.\d+Z$/, '').replace('T', ' ')}</span>
                    {authUser.user.isAuth && (authUser.user.user.PermissionsList?.indexOf('CanEditUsersComments') !== -1  || authUser.user.user.PermissionsList.indexOf('FullAccess') !== -1) &&
                        <div>
                            <button type="button" onClick={(e) => {setCurrentComment(comment); handleShow()}} className="btn btn-cyan btn-sm">Редактировать</button>
                            <button type="button" onClick={commentRemove} className="btn btn-danger btn-sm">Удалить</button>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
});

export default Comment;
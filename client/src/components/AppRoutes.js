import React, {useContext} from 'react';
import {AuthRoutes, PublicRoutes} from "../routes";
import {Switch, Route, Redirect} from 'react-router-dom';
import {MAIN_PAGE_ROUTE} from "../utils/consts";
import {Context} from "../index";


const AppRoutes = () => {
    const {user} = useContext(Context);
    return (
        <Switch>
            {user.isAuth && AuthRoutes.map(({path, component}) =>
                <Route key={path} path={path} component={component} exact/>
            )}
            {PublicRoutes.map(({path, component}) =>
                <Route key={path} path={path} component={component} exact/>
            )}
            <Redirect to={MAIN_PAGE_ROUTE}/>
        </Switch>
    );
};

export default AppRoutes;
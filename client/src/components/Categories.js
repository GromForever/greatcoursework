import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {ListGroup} from "react-bootstrap";

const Categories = observer(() => {
    const {news} = useContext(Context)
    return (
        <ListGroup>
            {news.categories.map(category =>
                <ListGroup.Item
                    style={{cursor: 'pointer'}}
                    action={category.IDCategory === news.selectedCategory.IDCategory}
                    onClick={() => news.setSelectedCategory(category)}
                    key={category.IDCategory}>
                        {category.CategoryName}
                </ListGroup.Item>
            )}
        </ListGroup>
    );
});

export default Categories;
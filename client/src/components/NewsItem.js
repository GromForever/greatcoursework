import React, {useContext} from 'react';
import {Button, Card} from "react-bootstrap";
import {useHistory} from "react-router-dom"
import {NEWS_ROUTE} from "../utils/consts";
import {Context} from "../index";
import {observer} from "mobx-react-lite";

const NewsItem = observer(({news}) => {
    const history = useHistory();
    const categories = useContext(Context)
    return (
        <Card style={{ width: '14rem' }} className="mx-4">
            <Card.Img variant="top" src={process.env.REACT_APP_API_URL + news.NewsImg} />
            <Card.Body>
                <Card.Title>{news.NewsTitle}</Card.Title>
                <Card.Text>
                    {news.NewsText.substring(0,30) + '...'}
                </Card.Text>
                <Button onClick={() => history.push(NEWS_ROUTE + '/' + news.IDNews)} variant="primary">Читать</Button>
                <div style={{fontWeight: "lighter"}}>
                    {categories.news.categories[news.categoryIDCategory-2].CategoryName}
                </div>
            </Card.Body>
        </Card>
    );
});

export default NewsItem;
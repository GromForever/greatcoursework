import React, {useEffect, useState} from 'react';
import {
    addRoleToUser,
    fetchRoles, getRoles,
    removeRoleFromUser
} from "../http/rolesAPI";
import {Button, Dropdown, Form, Modal, ModalBody, ModalFooter, ModalTitle} from "react-bootstrap";
import ModalHeader from "react-bootstrap/ModalHeader";
import DropdownToggle from "react-bootstrap/DropdownToggle";
import DropdownMenu from "react-bootstrap/DropdownMenu";
import DropdownItem from "react-bootstrap/DropdownItem";
import {fetchUsers} from "../http/userAPI";

const ModalUser = ({handleClose, show}) => {
    const [selectedUser, setSelectedUser] = useState({})
    const [roles, setRoles] = useState([])
    const [rolesOfUser, setRolesOfUser] = useState([])
    const [users, setUsers] = useState([])
    useEffect(() => {
        fetchUsers().then(data => setUsers(data)).catch(data => alert(data))
        fetchRoles().then(data => setRoles(data)).catch(data => alert(data))
    }, [])

    const getRolesByUser = async (user) => {
        const roles = await getRoles(user)
        setRolesOfUser(roles)
        return roles;
    }

    const CheckboxCheck = (role) => {
        for (let i = 0; i < rolesOfUser.length; i++) {
            let tempResult = rolesOfUser[i].userroles.roleIDRole === role.IDRole;
            if (tempResult === true) {
                return tempResult;
            }
        }
        return false;
    }

    const onChange = async (checkboxValue, role, user) => {
        if (checkboxValue) {
            addRoleToUser(user, role).then(data => {
                getRolesByUser(user).then(data1 => {setRolesOfUser(data1)}).catch(data => {alert(data)})
            }).catch(data => {alert(data)})
        } else {
            removeRoleFromUser(user, role).then(data => {
                getRolesByUser(user).then(data1 => {setRolesOfUser(data1)}).catch(data => {alert(data)})
            }).catch(data => {alert(data)})
        }
    }

    return (
        <div>
            <Modal show={show} onHide={handleClose}>
                <ModalHeader>
                    <ModalTitle>Изменение ролей пользователя</ModalTitle>
                </ModalHeader>
                <ModalBody>
                    <div>
                        <Dropdown className="mb-4">
                            <DropdownToggle>
                                {selectedUser.IDUser ? selectedUser.UserName : 'Выберите пользователя'}
                            </DropdownToggle>
                            <DropdownMenu>
                                {users && users.map(user => {
                                    return <DropdownItem onClick={(e) => {
                                        setSelectedUser(user);
                                        getRolesByUser(user.IDUser)}
                                    } key={user.IDUser}>{user.UserName}</DropdownItem>
                                })}
                            </DropdownMenu>
                        </Dropdown>
                        {roles && roles.map(role => {
                            return <Form.Check key={role.IDRole} type={"checkbox"} checked={CheckboxCheck(role)} onChange={(e) => onChange(e.target.checked, role.IDRole, selectedUser.IDUser)} label={role.RoleName}>

                            </Form.Check>
                        })}
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={handleClose}>Отмена</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
};

export default ModalUser;
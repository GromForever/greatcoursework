export const ADMIN_ROUTE = '/admin';
export const PROFILE_ROUTE = '/profile';
export const NEWS_ROUTE = '/news';
export const LOGIN_ROUTE = '/login';
export const REGISTER_ROUTE = '/register';
export const MAIN_PAGE_ROUTE = '/';
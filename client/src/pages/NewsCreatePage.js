import React, {useContext, useEffect} from 'react';
import {Context} from "../index";
import {Container, Form, Button, Dropdown} from "react-bootstrap"
import {useHistory} from "react-router-dom";
import {NEWS_ROUTE} from "../utils/consts";
import {CreateNews, getCategories} from "../http/newsAPI";
import {observer} from "mobx-react-lite";

const NewsCreatePage = observer(() => {
    const {user, news} = useContext(Context)
    let NewsTitle = '';
    let NewsText = '';
    let file = {};
    const history = useHistory();

    useEffect(() => {
        getCategories().then(data => news.setCategories(data)).catch(error => alert(error))
    }, [news])

    const addNews = async () => {
        const formData = new FormData()
        formData.append('NewsTitle', NewsTitle)
        formData.append('NewsText', NewsText)
        formData.append('NewsCategory', `${news.selectedCategory.IDCategory}`)
        formData.append('img', file)
        CreateNews(formData).then(data => history.push(NEWS_ROUTE)).catch(error => alert(error.response.data.message))
    }

    const sendFile = () => {
        console.log(file)
    }

    return (
        <Container>
        {(user.user.PermissionsList.indexOf('CreateNews') !== -1 || user.user.PermissionsList.indexOf('FullAccess') !== -1) ?
                <div>
                    <Form.Control className="mt-5" placeholder="Заголовок" id="NewsTitleID" onChange={(e) => {NewsTitle = e.target.value; console.log(NewsTitle)}}>

                    </Form.Control>
                    <Form.Control className="mt-3" placeholder="Текст" id="NewsTextID" onChange={(e) => {NewsText = e.target.value; console.log(NewsText)}}>

                    </Form.Control>

                    <Dropdown className="mt-2 mb-2">
                        <Dropdown.Toggle>{news.selectedCategory.CategoryName || "Выберите тип"}</Dropdown.Toggle>
                        <Dropdown.Menu>
                            {news.categories.map(category =>
                                <Dropdown.Item
                                    onClick={() => news.setSelectedCategory(category)}
                                    key={category.IDCategory}
                                >
                                    {category.CategoryName}
                                </Dropdown.Item>
                            )}
                        </Dropdown.Menu>
                    </Dropdown>

                    <Form.Control className="mt-3" type="file" placeholder="Выбирите файл" onChange={(e) => {file = e.target.files[0]; sendFile()}}>


                    </Form.Control>
                    <Button className="mt-5" onClick={() => addNews()}>Отправить</Button>
                </div>
         : 'У вас нет доступа к созданию новостей!'
        }
        </Container>
    );
});

export default NewsCreatePage;
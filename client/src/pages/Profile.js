import React, {useContext, useState} from 'react';
import {Button, Container, Modal, ModalBody, Form, ModalFooter, ModalTitle} from "react-bootstrap";
import {Context} from "../index";
import moment from "moment";
import {useHistory} from "react-router-dom";
import editIcon from "../img/edit.svg"
import {ADMIN_ROUTE, NEWS_ROUTE} from "../utils/consts";
import ModalHeader from "react-bootstrap/ModalHeader";
import {edit} from "../http/userAPI";
import {observer} from "mobx-react-lite";

const Profile = observer(() => {
    const {user} = useContext(Context)
    const [show, setShow] = useState(false);
    const [userData, setUserData] = useState({FirstName: user.user.FirstName, LastName: user.user.LastName, About: user.user.About, BirthDay: user.user.BirthDay})
    const [avatar, setAvatar] = useState()

    const editUser = async () => {
        const formData = new FormData()
        formData.append('FirstName',userData.FirstName)
        formData.append('LastName',userData.LastName)
        formData.append('About',userData.About)
        formData.append('BirthDay',userData.BirthDay)
        formData.append('img', avatar)
        const secret = await edit(formData);
        user.setUser(secret)
    }
    const onChange = ({target: {name, value}}) => {
        setUserData({...userData, [name]: value})
    }
    const handleShow = () => setShow(true);
    const handleClose = () => setShow (false)
    const history = useHistory()
    return (
        <Container>
        <div className="page-content page-container" id="page-content">
            <div className="padding">
                <div className="row container d-flex justify-content-center">
                    <div className="col-xl-12 col-md-12">
                        <div className="card user-card-full">
                            <div className="row m-l-0 m-r-0">
                                <div className="col-sm-4 bg-c-lite-green user-profile">
                                    <div className="card-block text-center text-white">
                                        <div className="m-b-25"><img
                                            src={process.env.REACT_APP_API_URL + user.user.userimg}
                                            className="img-radius rounded-circle" alt="" width={100}/></div>
                                        <h6 className="f-w-600">{user.user.FirstName + ' ' + user.user.LastName}</h6>
                                        <p>{user.user.About}</p>
                                    </div>
                                </div>
                                <div className="col-sm-8">
                                    <div className="card-block">
                                        <div className="d-flex justify-content-between">
                                            <h6 className="m-b-20 p-b-5 b-b-default f-w-600">Информация</h6>
                                                <img onClick={(e) => handleShow()} src={editIcon} width="20px" height="auto" alt=""/>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <p className="m-b-10 f-w-600">Email</p>
                                                <h6 className="text-muted f-w-400">{user.user.Email}</h6>
                                            </div>
                                            <div className="col-sm-6">
                                                <p className="m-b-10 f-w-600">Логин</p>
                                                <h6 className="text-muted f-w-400">{user.user.UserName}</h6>
                                            </div>
                                        </div>
                                        <h6 className="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">Состояние аккаунта</h6>
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <p className="m-b-10 f-w-600">Роли</p>
                                                <h6 className="text-muted f-w-400">{user.user.Roles.map(role => role + ' ')}</h6>
                                            </div>
                                            <div className="col-sm-6">
                                                <p className="m-b-10 f-w-600">День рождения</p>
                                                <h6 className="text-muted f-w-400">{user.user.BirthDay ? moment(user.user.BirthDay).format('L') : 'Не указано'}</h6>
                                            </div>
                                        </div>
                                        <ul className="social-link list-unstyled m-t-40 m-b-10">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div className="d-flex">
            {user.user.PermissionsList.indexOf('CreateNews') !== -1 || user.user.PermissionsList.indexOf('FullAccess') !== -1 ? <Button className="mr-4" onClick={() => history.push(NEWS_ROUTE + '/create')}>Создать новость</Button> : 'У вас нет прав на создание новостей!'}
            <br/>
            {user.user.PermissionsList.indexOf('AdminAccess') !== -1 || user.user.PermissionsList.indexOf('FullAccess') !== -1 ? <Button onClick={() => history.push(ADMIN_ROUTE)}>Админ панель</Button> : 'Вы не обладаете правами администратора!'}
            </div>
            <Modal show={show} onHide={(e) => handleClose()}>
                <ModalHeader>
                    <ModalTitle>Редактирование!</ModalTitle>
                </ModalHeader>
                <ModalBody>
                    <Form.Control placeholder="Имя" name="FirstName" className="mt-2" value={userData.FirstName} onChange={onChange}>

                    </Form.Control>
                    <Form.Control placeholder="Фамиля" name="LastName" className="mt-3" value={userData.LastName} onChange={onChange}>

                    </Form.Control>
                    <Form.Control placeholder="О себе" name="About" className="mt-3" value={userData.About} onChange={onChange}>

                    </Form.Control>
                    <Form.Control placeholder="День рождения" className="mt-3" name="BirthDay" value={userData.BirthDay} onChange={onChange}>

                    </Form.Control>
                    <Form.Control type="file" className="mt-3" name="img" onChange={(e) => {setAvatar(e.target.files[0])}}>

                    </Form.Control>
                    <ModalFooter>
                        <Button variant={"primary"} onClick={editUser}>Бутон</Button>
                        <Button variant={"secondary"} onClick={(e) => handleClose()}>Не бутон</Button>
                    </ModalFooter>
                </ModalBody>
            </Modal>
            </Container>
    );
});

export default Profile;
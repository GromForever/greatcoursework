import React from 'react';
import {Container} from "react-bootstrap";
import me from '../img/me.jpg'

const Main = () => {
    return (
            <Container>
                <div className="d-flex justify-content-between m-t-40">
                    <div style={{fontSize: "20px", fontWeight: 'bold'}}>
                        Великий и ужасный сайт it мастера
                        <div>
                            <small>Сайт был создан для деаонстраций знаний автора. Демонастрации реализации стандартных функций для сайта.</small>
                        </div>
                    </div>
                    <img src={me} alt="Моя фотка" width={300}/>
                </div>
            </Container>
    );
};

export default Main;
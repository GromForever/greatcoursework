import React, {useState} from 'react';
import {Button, Container} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import ModalPermissions from "../components/modalPermissions";
import ModalUser from "../components/modalUser";

const Admin = observer(() => {
    const [showPermissions, setShowPermissions] = useState(false)
    const [showUsers, setShowUsers] = useState(false)
    const handleClosePerm = () => setShowPermissions(false)
    const handleShowPerm = () => setShowPermissions(true)
    const handleCloseUser = () => setShowUsers(false)
    const handleShowUser = () => setShowUsers(true)
    return (
        <Container>
            <div className="d-flex justify-content-center mt-5">
                <div>
                    <h2 style={{textAlign: "center"}}>Панель администратора</h2>
                    <Button className="mr-4 mt-5" onClick={handleShowUser}>Редактировать роли пользователя</Button>
                    <Button className="mt-5" onClick={handleShowPerm}>Редактировать разрешения</Button>
                </div>
                <ModalPermissions show={showPermissions} handleClose={handleClosePerm} handleShow={handleShowPerm}/>
                <ModalUser show={showUsers} handleClose={handleCloseUser} handleShow={handleShowUser}/>
            </div>
        </Container>
    );
});

export default Admin;
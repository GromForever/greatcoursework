import React, {useContext, useEffect, useState} from 'react';
import {Button, Container, Form, Modal} from "react-bootstrap";
import {useParams} from 'react-router-dom'
import {CreateComment, EditCommentFromNews, fetchCommentsToNews, fetchOneNews} from "../http/newsAPI";
import img from '../img/test.jpg'
import {getUser} from "../http/userAPI";
import {observer} from "mobx-react-lite";
import '../style/style.css'
import {Context} from "../index";
import Comment from "../components/Comment";


const OneNews = observer(() => {
    const[news, setNews] = useState({})
    const[user, setUser] = useState({})
    const [show, setShow] = useState(false);
    let currentEditComment = '';

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [currentComment, setCurrentComment] = useState({})
    let comment = '';
    let commentCount = 0;
    const EditComment = async () => {
        const data = await EditCommentFromNews(currentComment.IDComment, currentEditComment);
        alert(data.Result)
        setShow(false)
    }
    const comments = useContext(Context)
    const {id} = useParams()
    const sendComment = () => {
        if (comment !== '' || comment >= 20) {
            CreateComment(id, comment, comments.user.user.id)
                .then((data) => {
                    comments.news.comments.push(data)
                    alert('Комментарий успешно отправлен!')
                })
                .catch(data => alert(data.response.data.message))
        } else {
            alert("Слишком короткий комментарий.")
        }
    }
    useEffect(() => {

        fetchOneNews(id).then(async (data) => {
            await setNews(data)
            getUser(data.userIDUser)
                .then(userData => setUser(userData))
                .catch(data => alert("Новость не найдена!"))
        })
        fetchCommentsToNews(id).then((data) => {
           comments.news.setComments(data)
        })
    }, [comments.news, id])
    return (
        <Container className="mt-3">
            <div className="d-flex justify-content-between">
                <div>
                    <h1>{news.NewsTitle}</h1>
                    <div style={{color: "grey"}} className="align-self-end">
                        {user.FirstName + ' ' + user.LastName  + ' aka ' + user.UserName}
                        <br/>
                        Дата создания: {news.CreationTime?.replace(/:\d{2}\.\d+Z$/, '').replace('T', ' ')}
                    </div>
                </div>
                <img src={process.env.REACT_APP_API_URL + news.NewsImg} width={250} alt="Фото" height={'auto'}/>
            </div>
            <div style={{fontSize: '18px'}} className="mt-3">
                {news.NewsText}
            </div>
            {comments.user.isAuth ? (comments.user.user.PermissionsList.indexOf('WriteComments') !== -1 || comments.user.user.PermissionsList.indexOf('FullAccess')) !== -1 ?
                <div className="mt-5">
                    Оставьте комментарий к записе
                    <Form.Control onChange={(e) => {comment = e.target.value}} id="CommentInput" as={"textarea"} rows={4} placeholder="Комментарий...">
                    </Form.Control>
                    <div className="text-right mt-3">
                        <Button className="btn " variant={"primary"} onClick={sendComment}>Отправить</Button>
                    </div>
                </div>
                :
                <div className="mt-4" style={{fontWeight: 'bold', fontSize: '18px'}}>У вас нет права на то, чтобы оставлять комментарии!</div>
                : <div className="mt-4" style={{fontWeight: 'bold', fontSize: '18px'}}>Авторизуйтесь, чтобы оставлять комментарии!</div>
            }
            <div className="card">
                <div className="card-body text-center">
                    <h4 className="card-title">Комментарии</h4>
                </div>
                <div className="comment-widgets">
                    {comments.news.comments.map(comment =>
                        <Comment currentComment={currentComment} setCurrentComment={setCurrentComment} handleShow={handleShow} key={comment.IDComment} commentCount={commentCount++} comment={comment}/>
                    )}
                </div>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Редактирование</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Control placeholder="Секретный инпут" onChange={(e)=>{currentEditComment=e.target.value; console.log(currentEditComment)}}>

                    </Form.Control>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={EditComment}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </Container>
    );
});

export default OneNews;
import React, {useContext, useEffect} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import Categories from "../components/Categories";
import NewsList from "../components/NewsList";
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {fetchNews, getCategories} from "../http/newsAPI";

const News = observer(() => {
    const {news} = useContext(Context);

    useEffect(() => {
        getCategories().then(data => news.setCategories(data)).catch(data => alert(data.message));
        fetchNews().then(data => news.setNews(data))
    }, [news])

    return (
        <Container>
            <Row className="mt-3">
                <Col md={3}>
                    <Categories/>
                </Col>
                <Col md={9}>
                    <NewsList/>
                </Col>
            </Row>
        </Container>
    );
});

export default News;
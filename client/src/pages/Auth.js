import React, {useContext, useState} from 'react';
import {Button, Card, Container, Form, Row} from "react-bootstrap";
import {NavLink, useHistory, useLocation} from "react-router-dom";
import {LOGIN_ROUTE, NEWS_ROUTE, REGISTER_ROUTE} from "../utils/consts";
import {login, registration} from "../http/userAPI";
import {Context} from "../index";

const Auth = () => {
    const location = useLocation();
    const {user} = useContext(Context)
    const history = useHistory()
    const isLogin = location.pathname === LOGIN_ROUTE;
    const [Email, setEmail] = useState('')
    const [Password, setPassword] = useState('')
    const [loginVar, setLoginVar] = useState('');

    const click = async () => {
        try {
            let data
            if (isLogin) {
                data = await login(Email, Password);
            } else {
                data = await registration(Email, Password, loginVar)
            }
            user.setUser(data)
            user.setIsAuth(true)
            history.push(NEWS_ROUTE);
        } catch (e) {
            alert(e.response.data.message);
        }
    }
    return (
        <Container
            style={{height: window.innerHeight - 54}}
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 600}} className="p-5">
                <h2 className="m-auto">{isLogin ? 'Авторизация' : 'Регистрация'}</h2>
                <Form className="d-flex flex-column">
                    <Form.Control placeholder="Введите ваш Email..." className="mt-4" value={Email} onChange={e => setEmail(e.target.value)}>

                    </Form.Control>
                    {!isLogin && <Form.Control placeholder="Введите ваш логин..." className="mt-4" value={loginVar} onChange={e => setLoginVar(e.target.value)}>

                    </Form.Control>}
                    <Form.Control placeholder="Введите ваш пароль..." className="mt-4"value={Password} onChange={e => setPassword(e.target.value)}>

                    </Form.Control>
                    <Row className="justify-content-between d-flex mt-4 pl-3 pr-3">
                        { isLogin ?
                            <div>Нет аккаунта? <NavLink to={REGISTER_ROUTE}>Зарегистрируйтесь!</NavLink></div>
                            :
                            <div>Уже зарегистрированны? <NavLink to={LOGIN_ROUTE}>Авторизуйтесь!</NavLink></div>
                        }
                        <Button onClick={click} variant={"outline-primary"}>{isLogin ? 'Войти' : 'Регистрация'}</Button>
                    </Row>
                </Form>
            </Card>
        </Container>
    );
};

export default Auth;
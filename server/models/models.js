const sequelize = require('../db')
const DataTypes = require('sequelize');
const {Sequelize} = require("sequelize");

const User = sequelize.define('user', {
    IDUser: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    FirstName: {type: DataTypes.STRING, defaultValue: "Anonymous"},
    LastName: {type: DataTypes.STRING, defaultValue: "Anonymous"},
    UserName: {type: DataTypes.STRING, unique: true, allowNull: false},
    Email: {type: DataTypes.STRING, isEmail: true, unique: true},
    EmailVerified: {type: DataTypes.BOOLEAN, defaultValue: false},
    EmailVerifyHash: {type: DataTypes.STRING, allowNull: true},
    Password: {type: DataTypes.STRING, allowNull: false},
    BirthDay: {type: DataTypes.DATEONLY, allowNull: true},
    RegisterDate: {type: DataTypes.DATE, allowNull: false, defaultValue: Sequelize.fn("NOW")},
    Blocked: {type: DataTypes.BOOLEAN, defaultValue: false},
    BlockTime: {type: DataTypes.DATE, allowNull: true},
    About: {type: DataTypes.STRING},
    userimg: {type: DataTypes.STRING, allowNull: true}
})

const Role = sequelize.define('role', {
    IDRole: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    RoleName: {type: DataTypes.STRING, allowNull: false, unique: true},
    RoleColor: {type: DataTypes.STRING, defaultValue: "Black"}
})

const Permissions = sequelize.define('permissions', {
    IDPermission: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    PermissionName: {type: DataTypes.STRING, unique: true}
})

const  RolePermissions = sequelize.define('rolepermissions', {
    IDRolePermission: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true}
})

const News = sequelize.define('news', {
    IDNews: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    CreationTime: {type: DataTypes.DATE, defaultValue: Sequelize.fn("NOW")},
    NewsTitle: {type: DataTypes.STRING, allowNull: false},
    NewsText: {type: DataTypes.STRING(2500)},
    NewsImg: {type: DataTypes.STRING, allowNull: true},
    Hidden: {type: DataTypes.BOOLEAN, defaultValue: false}
})

const UserRoles = sequelize.define('userroles', {
    IDMain: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
})

const Category = sequelize.define('categories', {
    IDCategory: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    CategoryName: {type: DataTypes.STRING, allowNull: false}
})


const Comments = sequelize.define('comments', {
    IDComment: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    CommentText: {type: DataTypes.STRING(600), allowNull: false},
    CommentDate: {type: DataTypes.DATE, defaultValue: Sequelize.fn("NOW")},
    Edited: {type: DataTypes.BOOLEAN, defaultValue: false},
    LastEditTime: {type: DataTypes.DATE, defaultValue: Sequelize.fn("NOW")},
})

User.belongsToMany(Role, {through: UserRoles})
Role.belongsToMany(User, {through: UserRoles})

News.belongsTo(User);
User.hasMany(News);

Permissions.belongsToMany(Role, {through: RolePermissions});
Role.belongsToMany(Permissions, {through: RolePermissions})

User.hasMany(Comments)

News.hasMany(Comments);
Comments.belongsTo(News)

Comments.hasOne(Comments, {as: "ParentComment", foreignKey: "ParentCommentFK"})
Comments.belongsTo(User);

Category.hasMany(News);
News.belongsTo(Category)

module.exports = {
    User,
    Role,
    UserRoles,
    News,
    Permissions,
    Comments,
    Category
}

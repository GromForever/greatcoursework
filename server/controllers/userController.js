const ApiError = require('../errorHandler/ApiError');
const bCrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User, Role} = require('../models/models');
const {validationResult} = require('express-validator')
const uuid = require('uuid')
const path = require('path')

GetUserInfo = async (req, res, next, candidate) => {
    const roles = (await candidate.getRoles({
        attributes: ['RoleName'],
        raw: true
    })).map(task => task.RoleName);
    let AllPermissions = [];
    for (let i = 0; i < roles.length; i++) {
        let currentRole = await Role.findOne({where: {RoleName: roles[i]}})
        AllPermissions =  Array.prototype.concat.apply(AllPermissions, (await currentRole.getPermissions({
            attributes: ['PermissionName'],
            raw: true}))
            .map(key => key.PermissionName));
    }
    if (!roles) {
        return res.status(500).json({message: "User error"})
    }
    return {AllPermissions, roles}
}

jwtGenerator = (IDUser, FirstName, LastName, UserName, Email, BirthDay, RegisterDate, About, Blocked, BlockTime, PermissionsList, Roles, userimg) => {
    const payLoad = {
        IDUser,
        FirstName,
        LastName,
        UserName,
        Email,
        BirthDay,
        RegisterDate,
        About,
        Blocked,
        BlockTime,
        PermissionsList,
        Roles,
        userimg
    }
    return jwt.sign(payLoad, process.env.JWTPASS, {expiresIn: "12h"})
}

class UserController{
    constructor() {

    }

    async getUsers(req, res) {
        const users = await User.findAll();
        if (!users) {
            res.status(404).json({message: 'Пользователи не найдены'})
        }
        res.json(users)
    }

    async editUser (req, res, next) {
        try {
            const user = await User.findOne({where: {IDUser: req.user.IDUser}})
            const {img} = req.files
            const imgName = uuid.v4() + '.jpg'
            await img.mv(path.resolve(__dirname, '..', 'static', imgName) )
            const result = await user.update({FirstName: req.body.FirstName, LastName: req.body.LastName, About: req.body.About, BirthDay: req.body.BirthDay, userimg: imgName})
            if (!result) {
                return next(ApiError.forbidden("Ошибка при выполнении запроса к базе данных"));
            }
            const additionalInfo = await GetUserInfo(req, res, next, result)
            result.dataValues.PermissionsList = additionalInfo.AllPermissions
            result.dataValues.Roles = additionalInfo.roles
            res.json(result)
        } catch (e) {
            console.log(e)
        }
    }

    async registration(req, res, next) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({Result: "Registration error", errors});
            }
            const {email, login, password, birthday, firstname, lastname} = req.body;
            let candidate = await User.findOne({where: {Email: email}});
            if (candidate) {
                return next(ApiError.forbidden("User with that Email already exists"));
            }
            candidate = await User.findOne({where: {UserName: login}});
            if (candidate) {
                return next(ApiError.forbidden("User with that username already exists"));
            }
            const HashedPassword = await bCrypt.hash(password, 7);
            const user = await User.create({
                Email: email,
                UserName: login.trim(),
                Password: HashedPassword,
                FirstName: firstname,
                LastName: lastname,
                BirthDay: birthday
            })
            await user.addRoles(await Role.findOne({where: {IDRole: 1}}));
            const UserInfo = await GetUserInfo(req, res, next, user)
            const token = jwtGenerator(user.IDUser, user.FirstName, user.LastName, user.UserName, user.Email, user.BirthDay, user.RegisterDate, user.About, user.Blocked, user.BlockTime, UserInfo.AllPermissions, UserInfo.roles, user.userimg);
            res.json({Result: "Success", token});
        } catch (e) {
            console.log(e.message)
        }
    }

    async login( req, res, next) {
        try {
            const {email, password} = req.body;
            const candidate = await User.findOne({where: {Email: email}});
            if(!candidate) {
                return next(ApiError.forbidden("Email or password is wrong", req, res));
            }
            const isPassValid = await bCrypt.compareSync(password, candidate.Password);
            if (!isPassValid) {
                return next(ApiError.forbidden("Email or password is wrong", req ,res));
            }
            const UserInfo = await GetUserInfo(req, res, next, candidate)
            const token = jwtGenerator(candidate.IDUser ,candidate.FirstName, candidate.LastName, candidate.UserName, candidate.Email, candidate.BirthDay, candidate.RegisterDate, candidate.About, candidate.Blocked, candidate.BlockTime, UserInfo.AllPermissions, UserInfo.roles, candidate.userimg);
            res.json({Result: "Successfully auth", token})
        } catch (e) {
            console.log(e.message)
        }
    }

    async getUser(req, res, next) {
        try {
            const {id} = req.params;
            if (!id) {
                res.status(401).json({result: 'error', message: 'id пользователя не указан'})
            }
            const user = await User.findOne({attributes: ['IDUser','FirstName', 'LastName', 'UserName', 'Email', 'BirthDay','RegisterDate', 'Blocked', 'About', 'userimg'], where: {IDUser: id}})
            if (!user) {
                res.status(401).json({result: 'error', message: 'Пользователь не найден.'})
            }
            res.json(user)
        } catch (e) {
            res.status(400).json({result: 'error', message: e.message})
        }
    }

    async check(req, res, next) {
        const token = jwtGenerator(req.user.IDUser, req.user.FirstName, req.user.LastName, req.user.UserName, req.user.Email, req.user.BirthDay, req.user.RegisterDate, req.user.About, req.user.Blocked, req.user.BlockTime, req.user.permissionsList, req.user.roles, req.user.userimg);
        res.json({token})
    }
}

module.exports = new UserController();
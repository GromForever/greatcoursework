const ApiError = require('../errorHandler/ApiError')
const {News, User} = require("../models/models");
const uuid = require('uuid')
const path = require('path')

class NewsController {
    constructor() {

    }

    async getNewsByID(req, res, next) {
        const {id} = req.params;
        if (!id) {
            return next(ApiError.badRequest('query have no ID argument!'))
        }
        let news = await News.findOne({where: {IDNews: id}})
        if (news) {
            res.json(news)
        } else res.json("Not Found");
    }

    async create(req, res, next) {
        if (req.body.NewsTitle === "" || req.body.NewsText === "") {
            return next(ApiError.badRequest('query body is invalid!'));
        }
        const {img} = req.files;
        let fileName = uuid.v4() + '.jpg'
        await img.mv(path.resolve(__dirname, '..', 'static', fileName) )
        let user = await User.findOne({where: {IDUser: req.user.IDUser}})
        let createdNews = await user.createNews({NewsTitle: req.body.NewsTitle, NewsText: req.body.NewsText, NewsImg: fileName, categoryIDCategory: req.body.NewsCategory})
        res.json(createdNews);
    }

    async getAll(req, res, next) {
        let allNews = await News.findAll()
         if (Object.keys(allNews).length === 0) {
             return next(ApiError.badRequest('News not found'));
         }
        res.json(allNews);
    };
}

module.exports = new NewsController();
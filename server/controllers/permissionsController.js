const {Permissions} = require('../models/models')

class PermissionsController {
    constructor() {

    }

    async getAll(req, res) {
        const permissions = await Permissions.findAll();
        if (!permissions) {
            res.status(404).json({message: "Не получено ни одного разрешения!"})
        }
        res.json(permissions)
    };

    async getByID(req, res) {};

    async remove(req, res) {};

    async create(req, res) {};
}

module.exports = new PermissionsController();
const {Category} = require('../models/models')

class CategoriesController {
    constructor() {

    }

    async getCategories(req, res) {
        const categories = await Category.findAll();
        if (!categories) {
            res.status(404).json({Result: 'error', message: 'Категорий не найдено'})
        }
        res.json(categories);
    }

    async createCategory(req, res) {
       const categoryName = req.body.categoryName;
       if (!categoryName) {
           res.status(404).json({Result: 'error', message: "Название категории в теле запроса отсутствует"})
       }
       const category = Category.create({CategoryName: categoryName});
       if (!category) {
           res.status(404).json({Result: 'error', message: "Создание категории окончилось ошибкой"})
       }
       res.json(category);
    }
}

module.exports = new CategoriesController();
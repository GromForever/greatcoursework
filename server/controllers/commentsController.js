const {Comments, News} = require('../models/models')
const ApiError = require('../errorHandler/ApiError')

class CommentsController {
    constructor() {

    }

    async getAll(req, res, next) {
        if (!req.countLimit) {
            req.countLimit = 10;
        }
        const comments = await Comments.findAll({limit: req.countLimit, order: ['IDComment', 'DESC']});
        if (!comments) {
            return next(ApiError.internal("Комментарии не найдены"))
        }
        res.json(comments);
    };

    async getByID(req, res, next) {
        if (!req.id) {
            return next(ApiError.badRequest("missing ID in request"))
        }
        const comments = await Comments.findOne({where: {IDComment: req.id}})
        if (!comments) {
            return res.status(404).json({message: "Comments not found"})
        }
        res.json(comments)
    };

    async editByID(req,res,next) {
        try {
            const {id} = req.params;
            if (!id) {
                res.status(401).json({Result: '', message: 'Некоректный запрос'})
            }
            const comText = req.body.commentText
            if (!comText) {
                res.status(401).json({Result: '', message: 'Отсутствует текст в теле запроса'})
            }
            const comment = await Comments.update({CommentText: comText},{where: {IDComment: id}})
            if (!comment) {
                res.status(401).json({Result: '', message: 'Комментарий не найден'})
            }
            res.status(200).json({Result: 'success'})
        } catch (e) {
            res.status(401).json({Result: '', message: 'не повезло не повезло'})
        }

    }

    async getByNews(req, res, next) {
        const {id} = req.params
        if (!id) {
            return next(ApiError.badRequest("news missing in request"))
        }
        if (!req.countLimit) {
            req.countLimit = 10;
        }
        const comments = await Comments.findAll({where: {newsIDNews: id}, order: ['createdAt']})
        if (!comments) {
            return res.status(404).json({message: "Comments from that news not found"})
        }
        res.json(comments)
    };

    async remove(req, res, next) {
        const {id} = req.body
        if (!id) {
            return next(ApiError.forbidden("ID не указан или указан неверно"))
        }
        const result = await Comments.destroy({where: {IDComment: id}})
        console.log(result)
        res.json({Result: 'Комментарий успешно удален!'})
    };

    async create(req, res) {
        const {commentText, IDNews} = req.body;
        const user = req.user.IDUser
        if (!req.user.IDUser) {
            return res.status(401).json({message: "Not Authorized"})
        }
        const news = await News.findOne({where: {IDNews: IDNews}})
        if (!news) {
            return res.status(500).json({message: "news not found"})
        }
        const comment = await news.createComment({CommentText: commentText, userIDUser: user})
        if (!comment) {
            res.status(401).json({message: "Something went wrong, comment has not created"})
        }
        res.json(comment);
    };
}

module.exports = new CommentsController();
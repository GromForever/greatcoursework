const {Role, Permissions, User} = require("../models/models");

class RoleController {
    constructor() {

    }

    async remove(req, res) {
        const role = req.body.role;
        const permission = req.body.permission;
        if (!role) {
            res.status(401).json({message: 'Роль не указана!'})
        }
        const findedRole = await Role.findOne({where: {IDRole: role}})
        if (!findedRole) {
            res.status(404).json({message: 'Роль не найдена'})
        }
        const findedPermission = await Permissions.findOne({where: {IDPermission: permission}})
        if (!findedPermission) {
            res.status(404).json({message: 'Разрешение не найдено!'})
        }
        const permissions = await findedRole.removePermissions(findedPermission);
        res.json(permissions)
    }

    async get(req, res) {
        const role = await Role.findAll();
        if (!role) {
            res.status(404).json({message: "Не получено ни одной роли!"})
        }
        res.json(role)
    }

    async getPermissionsByRole(req, res) {
        const role = req.body.role;
        if (!role) {
            res.status(401).json({message: 'Роль не указана!'})
        }
        const findedRole = await Role.findOne({where: {IDRole: role}})
        if (!findedRole) {
            res.status(404).json({message: 'Роль не найдена'})
        }
        const permissions = await findedRole.getPermissions();
        res.json(permissions)
    }

    async add(req, res) {
        const role = req.body.role;
        const permission = req.body.permission;
        if (!role) {
            res.status(401).json({message: 'Роль не указана!'})
        }
        const findedRole = await Role.findOne({where: {IDRole: role}})
        if (!findedRole) {
            res.status(404).json({message: 'Роль не найдена'})
        }
        const findedPermission = await Permissions.findOne({where: {IDPermission: permission}})
        if (!findedPermission) {
            res.status(404).json({message: 'Разрешение не найдено!'})
        }
        const permissions = await findedRole.addPermissions(findedPermission);
        res.json(permissions)
    }

    async getRolesByUser(req, res) {
        const user = req.body.user;
        if (!user) {
            res.status(404).json({message: 'Пользователь не указан в запросе!'})
        }
        const findedUser = await User.findOne({where: {IDUser: user}})
        if (!findedUser) {
            res.status(404).json({message: 'Пользователь не найден!'})
        }
        const roles = await findedUser.getRoles();
        res.json(roles)
    }

    async removeRoleFromUser(req, res) {
        const user = req.body.user;
        const role = req.body.role;
        if (!user) {
            res.status(404).json({message: 'Пользователь не указан в запросе!'})
        }
        if (!role) {
            res.status(404).json({message: 'Роль в запросе отсутствует'})
        }
        const findedUser = await User.findOne({where: {IDUser: user}})
        if (!findedUser) {
            res.status(404).json({message: 'Пользователь не найден!'})
        }
        const findedRole = await Role.findOne({where: {IDRole: role}})
        const result = await findedUser.removeRole(findedRole);
        res.json(result)
    }

    async addRoleFromUser(req, res) {
        const user = req.body.user;
        const role = req.body.role;
        if (!user) {
            res.status(404).json({message: 'Пользователь не указан в запросе!'})
        }
        if (!role) {
            res.status(404).json({message: 'Роль в запросе отсутствует'})
        }
        const findedUser = await User.findOne({where: {IDUser: user}})
        if (!findedUser) {
            res.status(404).json({message: 'Пользователь не найден!'})
        }
        const findedRole = await Role.findOne({where: {IDRole: role}})
        const result = await findedUser.addRole(findedRole);
        res.json(result)
    }

}

module.exports = new RoleController();
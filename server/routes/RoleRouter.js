const Router = require('express');
const router = Router();
const roleController = require('../controllers/roleController');

router.get('/get', roleController.get);
router.post('/add', roleController.add);
router.post('/getpermissions', roleController.getPermissionsByRole)
router.post('/remove', roleController.remove)
router.post('/getroles', roleController.getRolesByUser)
router.post('/addrole', roleController.addRoleFromUser)
router.post('/removerole', roleController.removeRoleFromUser)

module.exports = router;
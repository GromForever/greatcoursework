const Router = require('express');
const router = Router();
const userController = require("../controllers/userController")
const {check} = require('express-validator');
const authMiddleware = require('../middleware/AuthMiddleware')
const userMiddleware = require('../middleware/UserMiddleware')

router.post("/registration",[
    check('email', 'Email не может быть пустым').notEmpty(),
    check('login', 'Имя пользователя не может быть меньше 3 символов и больше 16').isLength({min: 3, max: 16}),
    check('password', 'Пароль не может быть меньше 6 символов и длинее 25').isLength({min: 6, max: 25})], userController.registration);
router.post("/login", userController.login);
router.get("/auth", authMiddleware, userMiddleware, userController.check);
router.put("/edit", [check('Firstname', 'Имя не указано!').notEmpty(),
    check('LastName', 'Фамилия не указана!').notEmpty(),
    check('About', 'Информация о себе не указано!').notEmpty(),
    check('BirthDay', 'День рождения не указан').notEmpty()],authMiddleware, userMiddleware, userController.editUser)
router.get('/get', userController.getUsers)
router.get('/:id', userController.getUser)

module.exports = router;
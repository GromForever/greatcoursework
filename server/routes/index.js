const Router = require('express');
const router = Router();
const userRouter = require("./userRouter");
const newsRouter = require("./NewsRouter");
const roleRouter = require("./RoleRouter");
const commentsRouter = require('./commentsRouter')
const categoryRouter = require('./categoryRouter')
const permissionsRouter = require('./PermissionsRouter')

router.use('/user', userRouter);
router.use("/news", newsRouter);
router.use("/role", roleRouter);
router.use("/comments", commentsRouter);
router.use("/category", categoryRouter)
router.use("/permissions", permissionsRouter)

module.exports = router;
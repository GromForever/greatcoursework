const Router = require('express');
const router = Router();
const categoryController = require('../controllers/categoriesController')
const authMiddleware = require('../middleware/AuthMiddleware')
const userMiddleware = require('../middleware/UserMiddleware')
const checkPermMiddleware = require('../middleware/CheckPermissionMiddleware')

router.get('/', categoryController.getCategories);
router.put('/create', authMiddleware, userMiddleware, checkPermMiddleware('CreateCategory'), categoryController.createCategory)

module.exports = router
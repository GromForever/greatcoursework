const Router = require('express');
const router = Router();
const newsController = require('../controllers/newsController')
const AuthMiddleware = require('../middleware/AuthMiddleware')
const UserMiddleware = require('../middleware/UserMiddleware')
const CheckPermMiddleware = require('../middleware/CheckPermissionMiddleware')

router.get('/:id', newsController.getNewsByID);
router.post("/status/create", AuthMiddleware, UserMiddleware, CheckPermMiddleware("CreateNews"), newsController.create);
router.get('/', newsController.getAll);

module.exports = router;
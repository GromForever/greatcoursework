const Router = require('express');
const router = Router();
const commentsController = require('../controllers/commentsController')
const authMiddleware = require('../middleware/AuthMiddleware')
const userMiddleware = require('../middleware/UserMiddleware')
const checkPermMiddleware = require('../middleware/CheckPermissionMiddleware')

router.get('/:id', commentsController.getByID);
router.get('/getbynews/:id', commentsController.getByNews)
router.post("/create", authMiddleware, userMiddleware, checkPermMiddleware('WriteComments'), commentsController.create);
router.get('/getall', commentsController.getAll);
router.post('/remove', authMiddleware, userMiddleware, checkPermMiddleware('CanEditUsersComments'), commentsController.remove);
router.put('/edit/:id', authMiddleware, userMiddleware, checkPermMiddleware('CanEditUsersComments'), commentsController.editByID)

module.exports = router;
const Router = require('express');
const router = Router();
const permissionsController = require('../controllers/permissionsController')

router.get('/', permissionsController.getByID);
router.post("/add", permissionsController.create);
router.get('/get', permissionsController.getAll);
router.get('/remove', permissionsController.remove);

module.exports = router;
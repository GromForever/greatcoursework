const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    if (req.method === "OPTIONS") {
        next();
    }
    try {
        if (!req.headers.authorization) {
            return res.status(401).json({message: "User not authorized"})
        }
        const token = req.headers.authorization.split(' ') [1]; //Bearer fasdhfjlasjjl
        if (!token) {
            return res.status(401).json({message: "User not authorized"})
        }
        req.user = jwt.verify(token, process.env.JWTPASS);
        next()
    } catch (e) {
        res.status(401).json({message: "Not authorized" + e.message})
    }
}
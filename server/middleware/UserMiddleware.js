const {User, Role} = require('../models/models')

module.exports = async (req, res, next) => {
        try {
            const userID = req.user.IDUser;
            if (!userID) {
                return res.status(401).json({message: "12345"})
            }
            const user = await User.findOne({where: {IDUser: userID}})
            if (!user) {
                return res.status(404).json({message: "User not found"})
            }
            const roles = (await user.getRoles({
                attributes: ['RoleName'],
                raw: true
            })).map(task => task.RoleName);
            let AllPermissions = [];
            for (let i = 0; i < roles.length; i++) {
               let currentRole = await Role.findOne({where: {RoleName: roles[i]}})
               AllPermissions =  Array.prototype.concat.apply(AllPermissions, (await currentRole.getPermissions({
                    attributes: ['PermissionName'],
                    raw: true}))
                    .map(key => key.PermissionName));
            }
            if (!roles) {
                return res.status(500).json({message: "User error"})
            }
            req.user = user
            req.user.permissionsList = AllPermissions;
            req.user.roles = roles;
            next();
        } catch (e) {
            console.log(e.message);
        }
    }

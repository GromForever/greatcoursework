module.exports = function (permission) {
    return function (req, res, next) {
        if (req.method === "OPTIONS") {
            next();
        }
        try {
            if (!req.user.IDUser) {
                return res.status(401).json({message: "User not authorized"})
            }
            if (!req.user.permissionsList) {
                return res.status(403).json({message: "Access denied"})
            }
            const requiredPermission = req.user.permissionsList.find(key => key === permission || key === 'FullAccess' )
            if (!requiredPermission) {
                return res.status(403).json({message: "Access denied"})
            }
            next();
        } catch (e) {
            res.status(401).json({message: "Not authorized123" + e.message})
        }
    }
}